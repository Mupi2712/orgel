export const environment = {
  production: true,
  mqtt: {
    server: 'tccloudmpieper.ddns.net',
    protocol: 'wss',
    port: 1884
  }
};
