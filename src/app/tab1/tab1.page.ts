import {Component} from '@angular/core';
import {MqttService} from 'ngx-mqtt';
import {timeout} from 'rxjs/operators';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  overMqtt = true;
  state = 'MQTT';
  bManPlay = false;

  constructor(private mqttService: MqttService) {
    document.addEventListener('keydown', ev => {
      if (this.bManPlay) {
        this.keydown(ev);
      }
    });
    document.addEventListener('keyup', ev => {
      if (this.bManPlay) {
        this.keyup(ev);
      }
    });
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  async selfTest() {
    document.getElementById('ListenArea').innerText = 'Selftest';
    this.send('t');
    await this.delay(1000);
    this.send('');
    console.log('Selftest!');
  }

  async playFirmware() {
    document.getElementById('ListenArea').innerText = 'Play internal song!';
    this.send('p');
    await this.delay(1000);
    this.send('');
    console.log('Playing melody from Firmware');
  }

  playNote(note: number, noteChar: string) {
    document.getElementById('ListenArea').innerText = noteChar;
    this.send(note.toString());
    console.log('Playing melody from Firmware');
  }

  send(note: string) {
    if (this.overMqtt) {
      this.mqttService.unsafePublish('orgel/modes', note, {qos: 1, retain: false});
    }
  }

  manPlay(toMain: boolean) {
    document.getElementById('selftest').hidden = !toMain;
    document.getElementById('playFirmware').hidden = !toMain;
    document.getElementById('manplay').hidden = !toMain;
    document.getElementById('backToMainMenue').hidden = toMain;
    document.getElementById('Manual_Mode').hidden = toMain;
    if (!toMain) {
      this.bManPlay = true;
      document.getElementById('toggle').style.top = '0';
    } else {
      this.bManPlay = false;
    }
    /*
    document.getElementById('playFile').hidden = !toMain;
    document.getElementById('dirOut').hidden = true;
    document.getElementById('dirIn').hidden = true;
    document.getElementById('submit').hidden = true;*/
    document.getElementById('ListenArea').innerText = '';
  }

  changeState() {
    if (this.overMqtt) {
      this.state = 'MQTT';
    } else {
      this.state = 'OPC-UA';
    }
  }

  keydown(event: any) {
    console.log(event);
    this.send(event.key.toString());
  }

  keyup(event: any) {
    console.log(event);
    this.playNote(0, '');
  }
}
